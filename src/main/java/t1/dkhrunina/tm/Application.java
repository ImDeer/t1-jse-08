package t1.dkhrunina.tm;

import t1.dkhrunina.tm.api.ICommandRepository;
import t1.dkhrunina.tm.constant.ArgumentConst;
import t1.dkhrunina.tm.constant.CommandConst;
import t1.dkhrunina.tm.model.Command;
import t1.dkhrunina.tm.repository.CommandRepository;
import t1.dkhrunina.tm.util.TerminalUtil;

import static t1.dkhrunina.tm.util.FormatUtil.formatBytes;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();
    
    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("*** Welcome to Task Manager ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nEnter command: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length < 1) return;
        processArgument(args[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.out.println("\n[ERROR]");
        System.err.println("Input arguments are not correct.");
        System.exit(1);
    }

    private static void showSystemInfo() {
        System.out.println("\n[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("Processors: " + processorCount);
        System.out.println("Max memory: " + formatBytes(maxMemory));
        System.out.println("Total memory: " + formatBytes(totalMemory));
        System.out.println("Free memory: " + formatBytes(freeMemory));
        System.out.println("Used memory: " + formatBytes(usedMemory));
    }

    private static void showCommandError() {
        System.out.println("\n[ERROR]");
        System.err.println("Command is not correct.");
    }

    private static void processCommand(final String arg) {
        switch (arg) {
            case CommandConst.VERSION: {
                showVersion();
                break;
            }
            case CommandConst.ABOUT: {
                showAbout();
                break;
            }
            case CommandConst.HELP: {
                showHelp();
                break;
            }
            case CommandConst.INFO: {
                showSystemInfo();
                break;
            }
            case CommandConst.EXIT: {
                exit();
                break;
            }
            default: {
                showCommandError();
                break;
            }
        }
    }

    private static void processArgument(final String arg) {
        switch (arg) {
            case ArgumentConst.VERSION: {
                showVersion();
                break;
            }
            case ArgumentConst.ABOUT: {
                showAbout();
                break;
            }
            case ArgumentConst.HELP: {
                showHelp();
                break;
            }
            case ArgumentConst.INFO: {
                showSystemInfo();
                break;
            }
            default: {
                showArgumentError();
                break;
            }
        }
    }

    private static void showVersion() {
        System.out.println("\n[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showAbout() {
        System.out.println("\n[ABOUT]");
        System.out.println("Name: Daria Khrunina");
        System.out.println("Email: dkhrunina@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("\n[HELP]");
        for (Command command: COMMAND_REPOSITORY.getCommands()) System.out.println(command);
    }
    
}
