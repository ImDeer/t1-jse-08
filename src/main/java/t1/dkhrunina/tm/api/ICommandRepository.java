package t1.dkhrunina.tm.api;

import t1.dkhrunina.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}