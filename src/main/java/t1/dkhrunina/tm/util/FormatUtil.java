package t1.dkhrunina.tm.util;

import java.text.DecimalFormat;

public interface FormatUtil {

    DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");

    double KILOBYTE = 1024.0;

    double MEGABYTE = KILOBYTE * 1024;

    double GIGABYTE = MEGABYTE * 1024;

    double TERABYTE = GIGABYTE * 1024;

    static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < KILOBYTE)) {
            return bytes + " B";
        } else if ((bytes >= KILOBYTE) && (bytes < MEGABYTE)) {
            return DECIMAL_FORMAT.format(bytes / KILOBYTE) + " KB";
        } else if ((bytes >= MEGABYTE) && (bytes < GIGABYTE)) {
            return DECIMAL_FORMAT.format(bytes / MEGABYTE) + " MB";
        } else if ((bytes >= GIGABYTE) && (bytes < TERABYTE)) {
            return DECIMAL_FORMAT.format(bytes / GIGABYTE) + " GB";
        } else if (bytes >= TERABYTE) {
            return DECIMAL_FORMAT.format(bytes / TERABYTE) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }

}